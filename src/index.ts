import * as stats from './stats';
import * as fs from 'fs';
import { Dex, Species, toID } from '@pkmn/sim';
import { MoveClient, Pokemon, PokemonClient, PokemonSpecies } from 'pokenode-ts';
import axios from 'axios';
import { Locale } from './lang';

const speciesHolder = new Map<string, stats.Species>()
const formsHolder = new Map<string, stats.Form[]>()
const evolutionMovesHolder = new Map<string, string[]>()

const removedMoveDescriptions = [
    '사용할 수 없는 기술입니다. 다시 배우게 할 수 없지만 기술을 잊게 하는 것을 권장합니다.',
    '無法使用此招式。 雖然忘記後將無法再想起來， 但還是建議忘記此招式。',
    'Cette capacité ne peut pas être utilisée. Il est recommandé de l’oublier, même s’il sera impossible de se la remémorer une fois oubliée.',
    'Diese Attacke kann nicht eingesetzt werden. Du solltest dein Pokémon sie vergessen lassen. Beachte aber, dass es sich danach nicht wieder an sie erinnern kann.',
    'Este movimiento no se puede usar, por lo que sería mejor olvidarlo, aunque eso implique que no se pueda recordar posteriormente.',
    'Questa mossa non può essere usata. È consigliabile farla dimenticare al Pokémon. Tuttavia, una volta dimenticata, non potrà più essere ricordata.',
    'This move can’t be used. It’s recommended that this move is forgotten. Once forgotten, this move can’t be remembered.',
    'この技は　使えません 思い出すことが　できなくなりますが 技を　忘れることを　おすすめします',
    '无法使用这个招式。 虽然忘记之后就再也想不起来了， 但还是建议忘记这个招式。'
]

// Used for Special Ability data to transform them into proper forms
const GIMMICK_RESOLVERS = new Map<string, (species: stats.Species) => void>([
    ['Greninja', species => resolveAshGreninja(species)],
    ['Zygarde', species => resolveZygardePowerConstruct(species)],
    ['Rockruff', species => resolveOwnTempoRockruff(species)],
    ['Lycanroc', species => resolveDuskLycanroc(species)]
])

const CONDITION_TO_REQUIREMENT = new Map<string, stats.EvolutionRequirement[]>([
    ['at night', [new stats.TimeRangeRequirement('night')]],
    ['during the day', [new stats.TimeRangeRequirement('day')]],
    ['with an atk stat > its def stat', [new stats.StatRatioRequirement('ATTACK_HIGHER')]],
    ['with an atk stat < its def stat', [new stats.StatRatioRequirement('DEFENCE_HIGHER')]],
    ['with an atk stat equal to its def stat', [new stats.StatRatioRequirement('EQUAL')]],
    ['with a dark-type in the party', [new stats.PartyMemberRequirement('type=dark', true)]],
    ['with the console turned upside-down', [new stats.PropertiesRequirement('nickname=jeb')]],
    // We don't need to add any other conditions as we treat the rockruff as a split form all we need is the dusk time
    ['from a special rockruff', [new stats.TimeRangeRequirement('dusk')]],
    ['with a remoraid in party', [new stats.PartyMemberRequirement('remoraid', true)]],
    ['with a fairy-type move and two levels of affection', [new stats.IntRequirement('friendship', 160), new stats.MoveTypeRequirement('fairy')]],
    // lazy cheat for these 2, they're a part of the trade evolution params instead
    ['with a karrablast', []],
    ['with a shelmet', []],
    ['land 3 critical hits in 1 battle', [new stats.IntRequirement('battle_critical_hits', 3)]],
    ['have 49+ hp lost and walk under stone sculpture in dusty bowl', [new stats.IntRequirement('damage_taken', 49)]],
    // TODO design council is yet to decide solution here
    ['spin while holding a sweet', []],
    ['use agile style psyshield bash 20 times', [new stats.UseMoveRequirement('Psyshield Bash', 20)]],
    ['receive 294+ recoil without fainting', [new stats.IntRequirement('recoil', 294)]],
    ['use strong style barb barrage 20 times', [new stats.UseMoveRequirement('Barb Barrage', 20)]],
    ['walk 1000 steps in let\'s go', [new stats.IntRequirement('walked_steps', 1000)]],
    ['use rage fist 20 times and level-up', [new stats.UseMoveRequirement('Rage Fist', 20)]],
    ['defeat 3 bisharp leading pawniard and level-up', [new stats.DefeatRequirement(new stats.PokemonProperties('bisharp', `held_item=cobblemon:leaders_crest`), 3)]],
    // TODO design council is yet to decide solution here
    ['level up with 999 coins in the bag', []]
])

const BABY_POKEMON = ['Pichu', 'Cleffa', 'Igglybuff', 'Togepi', 'Tyrogue', 'Togepi', 'Tyrogue', 'Smoochum', 'Elekid', 'Magby', 'Azurill', 'Wynaut', 'Budew', 'Chingling', 'Bonsly', 'Mime Jr.', 'Happiny', 'Munchlax', 'Riolu', 'Mantyke', 'Toxel']
const REGIONAL_FORMS = ['Alola', 'Galar', 'Galar-Zen', 'Hisui', 'Paldea', 'Paldea-Combat', 'Paldea-Blaze', 'Paldea-Aqua']
const GEN_1_DEX_CAP = 151
const GEN_2_DEX_CAP = 251
const GEN_3_DEX_CAP = 386
const GEN_4_DEX_CAP = 493
const GEN_5_DEX_CAP = 649
const GEN_6_DEX_CAP = 721
const GEN_7_DEX_CAP = 809
const GEN_8_DEX_CAP = 905
const GEN_9_DEX_CAP = 1010

const KEEP_EXISTING_EVOLUTIONS = true

const DATA_DIR = 'data/cobblemon'
const ASSETS_DIR = 'assets/cobblemon'
const SPECIES_DIR = `${DATA_DIR}/species`
const LANG_DIR = `${ASSETS_DIR}/lang`
const EXISTING_DATA_URL = 'https://gitlab.com/cable-mc/cobblemon/-/raw/main/common/src/main/resources'
const POKEAPI_KEY_CONVERTER = new Map<string, string>([
    ['Raticate-Alola-Totem', 'Raticate-Alola'],
    ['Pikachu-Original', 'Pikachu'],
    ['Pikachu-Hoenn', 'Pikachu'],
    ['Pikachu-Sinnoh', 'Pikachu'],
    ['Pikachu-Unova', 'Pikachu'],
    ['Pikachu-Kalos', 'Pikachu'],
    ['Pikachu-Alola', 'Pikachu'],
    ['Pikachu-Partner', 'Pikachu'],
    ['Pikachu-World', 'Pikachu'],
    ['Farfetch\u{2019}d', 'Farfetchd'],
    ['Farfetch\u{2019}d-Galar', 'Farfetchd-Galar'],
    ['Marowak-Alola-Totem', 'Marowak-Alola'],
    ['Mr. Mime', 'Mr-Mime'],
    ['Mr. Mime-Galar', 'Mr-Mime-Galar'],
    ['Tauros-Paldea-Combat', 'Tauros-Paldea-Combat-Breed'],
    ['Tauros-Paldea-Blaze', 'Tauros-Paldea-Blaze-Breed'],
    ['Tauros-Paldea-Aqua', 'Tauros-Paldea-Aqua-Breed'],
    ['Pichu-Spiky-eared', 'Pichu'],
    ['Deoxys', 'Deoxys-Normal'],
    ['Pichu-Spiky-eared', 'Pichu'],
    ['Wormadam', 'Wormadam-Plant'],
    ['Cherrim-Sunshine', 'Cherrim'],
    ['Mime Jr.', 'Mime-Jr'],
    ['Giratina', 'Giratina-Altered'],
    ['Shaymin', 'Shaymin-Land'],
    ['Arceus-Bug', 'Arceus'],
    ['Arceus-Dark', 'Arceus'],
    ['Arceus-Dragon', 'Arceus'],
    ['Arceus-Electric', 'Arceus'],
    ['Arceus-Fairy', 'Arceus'],
    ['Arceus-Fighting', 'Arceus'],
    ['Arceus-Fire', 'Arceus'],
    ['Arceus-Flying', 'Arceus'],
    ['Arceus-Ghost', 'Arceus'],
    ['Arceus-Grass', 'Arceus'],
    ['Arceus-Ground', 'Arceus'],
    ['Arceus-Ice', 'Arceus'],
    ['Arceus-Poison', 'Arceus'],
    ['Arceus-Psychic', 'Arceus'],
    ['Arceus-Rock', 'Arceus'],
    ['Arceus-Steel', 'Arceus'],
    ['Arceus-Water', 'Arceus'],
    ['Basculin', 'Basculin-Red-Striped'],
    ['Darmanitan', 'Darmanitan-Standard'],
    ['Darmanitan-Galar', 'Darmanitan-Galar-Standard'],
    ['Tornadus', 'Tornadus-Incarnate'],
    ['Thundurus', 'Thundurus-Incarnate'],
    ['Landorus', 'Landorus-Incarnate'],
    ['Keldeo', 'Keldeo-Ordinary'],
    ['Meloetta', 'Meloetta-Aria'],
    ['Genesect-Douse', 'Genesect'],
    ['Genesect-Shock', 'Genesect'],
    ['Genesect-Burn', 'Genesect'],
    ['Genesect-Chill', 'Genesect'],
    ['Vivillon-Fancy', 'Vivillon'],
    ['Vivillon-Pokeball', 'Vivillon'],
    ['Flabébé', 'Flabebe'],
    ['Meowstic', 'Meowstic-Male'],
    ['Meowstic-F', 'Meowstic-Female'],
    ['Aegislash', 'Aegislash-Shield'],
    ['Pumpkaboo', 'Pumpkaboo-Average'],
    ['Gourgeist', 'Gourgeist-Average'],
    ['Xerneas-Neutral', 'Xerneas'],
    ['Zygarde', 'Zygarde-50'],
    ['Zygarde-10%', 'Zygarde-10'],
    ['Oricorio', 'Oricorio-Baile'],
    ['Oricorio-Pa\'u', 'Oricorio-Pau'],
    ['Lycanroc', 'Lycanroc-Midday'],
    ['Wishiwashi', 'Wishiwashi-Solo'],
    ['Type: Null', 'Type-Null'],
    ['Silvally-Bug', 'Silvally'],
    ['Silvally-Dark', 'Silvally'],
    ['Silvally-Dragon', 'Silvally'],
    ['Silvally-Electric', 'Silvally'],
    ['Silvally-Fairy', 'Silvally'],
    ['Silvally-Fighting', 'Silvally'],
    ['Silvally-Fire', 'Silvally'],
    ['Silvally-Flying', 'Silvally'],
    ['Silvally-Ghost', 'Silvally'],
    ['Silvally-Grass', 'Silvally'],
    ['Silvally-Ground', 'Silvally'],
    ['Silvally-Ice', 'Silvally'],
    ['Silvally-Poison', 'Silvally'],
    ['Silvally-Psychic', 'Silvally'],
    ['Silvally-Rock', 'Silvally'],
    ['Silvally-Steel', 'Silvally'],
    ['Silvally-Water', 'Silvally'],
    ['Minior', 'Minior-Red'],
    ['Minior-Meteor', 'Minior-Red-Meteor'],
    ['Mimikyu', 'Mimikyu-Disguised'],
    ['Mimikyu-Totem', 'Mimikyu-Totem-Disguised'],
    ['Mimikyu-Busted-Totem', 'Mimikyu-Totem-Busted'],
    ['Tapu Koko', 'Tapu-Koko'],
    ['Tapu Lele', 'Tapu-Lele'],
    ['Tapu Bulu', 'Tapu-Bulu'],
    ['Tapu Fini', 'Tapu-Fini'],
    ['Necrozma-Dusk-Mane', 'Necrozma-Dusk'],
    ['Necrozma-Dawn-Wings', 'Necrozma-Dawn'],
    ['Toxtricity', 'Toxtricity-Amped'],
    ['Toxtricity-Gmax', 'Toxtricity-Amped-Gmax'],
    ['Sinistea-Antique', 'Sinistea'],
    ['Polteageist-Antique', 'Polteageist'],
    ['Sirfetch\u{2019}d', 'Sirfetchd'],
    ['Mr. Rime', 'Mr-Rime'],
    ['Eiscue', 'Eiscue-Ice'],
    ['Indeedee', 'Indeedee-Male'],
    ['Indeedee-F', 'Indeedee-Female'],
    ['Morpeko', 'Morpeko-Full-Belly'],
    ['Urshifu', 'Urshifu-Single-Strike'],
    ['Urshifu-Gmax', 'Urshifu-Single-Strike-Gmax'],
    ['Basculegion', 'Basculegion-Male'],
    ['Basculegion-F', 'Basculegion-Female'],
    ['Enamorus', 'Enamorus-Incarnate'],
    ['Oinkologne-F', 'Oinkologne-Female'],
    ['Maushold-Four', 'Maushold'],
    ['Squawkabilly-Blue', 'Squawkabilly-Blue-Plumage'],
    ['Squawkabilly-Yellow', 'Squawkabilly-Yellow-Plumage'],
    ['Squawkabilly-White', 'Squawkabilly-White-Plumage'],
    ['Great Tusk', 'Great-Tusk'],
    ['Scream Tail', 'Scream-Tail'],
    ['Brute Bonnet', 'Brute-Bonnet'],
    ['Flutter Mane', 'Flutter-Mane'],
    ['Slither Wing', 'Slither-Wing'],
    ['Sandy Shocks', 'Sandy-Shocks'],
    ['Iron Treads', 'Iron-Treads'],
    ['Iron Bundle', 'Iron-Bundle'],
    ['Iron Hands', 'Iron-Hands'],
    ['Iron Jugulis', 'Iron-Jugulis'],
    ['Iron Moth', 'Iron-Moth'],
    ['Iron Thorns', 'Iron-Thorns'],
    ['Roaring Moon', 'Roaring-Moon'],
    ['Iron Valiant', 'Iron-Valiant'],
])
const LANG_CONVERTER = new Map<string, Locale>([
    ['ko', 'ko_kr'],
    ['zh-Hant', 'zh_hk'],
    ['fr', 'fr_fr'],
    ['de', 'de_de'],
    ['es', 'es_es'],
    ['it', 'it_it'],
    ['en', 'en_us'],
    ['cs', 'cs_cz'],
    ['ja', 'ja_jp'],
    ['zh-Hans', 'zh_cn'],
    ['pt-BR', 'pt_br'],
])

type Export = Record<string, any>

start()

async function start() {
    const client = new PokemonClient()
    console.info('Starting to scrape ability lang')
    await extractAbilityLang(client)
    console.info('Finished scraping ability lang')

    console.info('Starting to scrape move lang')
    await extractMoveLang()
    console.info('Finished scraping move lang')

    console.info('Starting to scrape species data and lang')
    const speciesLocale = new Map<Locale, Map<string, string>>()
    for (const species of Dex.species.all()) {
        // Skips Showdown Fakemon and MissingNo.
        if (species.num <= 0 || species.num > GEN_9_DEX_CAP) {
            continue
        }
        const pokeApiId = POKEAPI_KEY_CONVERTER.get(species.name)?.toLocaleLowerCase() ?? species.name.toLocaleLowerCase()
        await client.getPokemonByName(pokeApiId)
            .then((pokemonData) =>
                client.getPokemonSpeciesById(species.num)
                .then((speciesData) => extractData(species, pokemonData, speciesData, speciesLocale))
                .catch((error) => {
                    console.warn(`Error finding Species data for %s retrieving data without PokeAPI Species`, species.name)
                    console.error(error)
                    extractData(species, pokemonData, undefined, speciesLocale)
                })
            )
            .catch((_) => {
                    console.warn(`Error finding Pokémon data for %s retrieving data without PokeAPI Pokémon and Species`, species.name)
                    extractData(species, undefined, undefined, speciesLocale)
            })
    }
    writeLang('species', speciesLocale)
    speciesHolder.forEach((species, key) => {
        appendEvolutionMoves(species)
        const dir = `${SPECIES_DIR}/generation${species.generation}`
        const fileName = `${toID(species.name)}.json`
        const assetUrl = `${EXISTING_DATA_URL}/${SPECIES_DIR}/generation${species.generation}/${fileName}`
        species.forms = formsHolder.get(key)
        species.forms?.forEach(form => appendEvolutionMoves(form))
        species.generation = undefined
        GIMMICK_RESOLVERS.get(key)?.call(species, species)
        attemptAppendAndWrite(species, assetUrl, dir, fileName)
    })
    console.info('Finished scraping species data and lang')
}

async function extractAbilityLang(client: PokemonClient) {
    const abilityLocale = new Map<Locale, Map<string, string>>()
    for (const ability of Dex.abilities.all()) {
        if (ability.id === 'noability' || ability.isNonstandard === 'CAP' || ability.isNonstandard === 'Custom' || ability.gen !== 9) {
            continue
        }
        let pokeApiId = toPokeApiID(ability.name)
        await client.getAbilityByName(pokeApiId)
            .then((abilityData) => {
                abilityData.names.forEach(entry => appendLangEntry(entry.language.name, abilityLocale, `cobblemon.ability.${ability.id}`, entry.name))
                abilityData.flavor_text_entries.forEach(entry => appendLangEntry(entry.language.name, abilityLocale, `cobblemon.ability.${ability.id}.desc`, entry.flavor_text.replace(/\n/g, ' ')))
            })
            .catch((_) => console.log('Cannot find %s with the PokeAPI ID %s', ability.name, pokeApiId))
    }
    writeLang('abilities', abilityLocale)
}

async function extractMoveLang() {
    const client = new MoveClient()
    const moveLocale = new Map<Locale, Map<string, string>>()
    for (const move of Dex.moves.all()) {
        if (move.isNonstandard === 'CAP' || move.isNonstandard === 'Custom') {
            continue
        }
        let pokeApiId = move.id === 'visegrip' ? 'vice-grip' : toPokeApiID(move.name)
        await client.getMoveByName(pokeApiId)
            .then((moveData) => {
                moveData.names.forEach(entry => appendLangEntry(entry.language.name, moveLocale, `cobblemon.move.${move.id}`, entry.name))
                moveData.flavor_text_entries.filter(entry => !removedMoveDescriptions.includes(entry.flavor_text.replace(/\n/g, ' ')))
                    .forEach(entry => appendLangEntry(entry.language.name, moveLocale, `cobblemon.move.${move.id}.desc`, entry.flavor_text.replace(/\n/g, ' ')))
            })
            .catch((_) => console.log('Cannot find %s with the PokeAPI ID %s', move.name, pokeApiId))
    }
    writeLang('moves', moveLocale)
}

function appendLangEntry(pokeApiLocale: string, langHolder: Map<Locale, Map<string, string>>, key: string, value: string): boolean {
    const locale = LANG_CONVERTER.get(pokeApiLocale)
    if (locale) {
        const map = langHolder.get(locale) ?? new Map<string, string>()
        map.set(key, value)
        langHolder.set(locale, map)
        return true
    }
    return false
}

function writeLang(subDir: string, langHolder: Map<Locale, Map<string, string>>) {
    const langDir = `${LANG_DIR}/${subDir}`
    fs.mkdirSync(langDir, { recursive: true })
    langHolder.forEach((value, key) => {
        const fileName = `${key}.json`
        const json = JSON.stringify(Object.fromEntries(value), null, 2)
        fs.writeFileSync(`${langDir}/${fileName}`, json, 'utf-8')
    })
}

async function attemptAppendAndWrite(species: stats.Species, assetUrl: string, dir: string, fileName: string) {
    await axios.get(assetUrl)
        .then((reply) => {
            const exporting: Export = {}
            Object.entries(reply.data).forEach(entry => exporting[entry[0]] = entry[1])
            if (KEEP_EXISTING_EVOLUTIONS) {
                const asSpecies = reply.data as stats.Species
                species.evolutions = asSpecies.evolutions
                asSpecies.forms?.forEach(form => {
                    species.forms?.filter(element => element.name === form.name)
                        .forEach(element => element.evolutions = form.evolutions)
                })
            }
            Object.entries(species).forEach(entry => exporting[entry[0]] = entry[1])
            write(exporting, dir, fileName)
        })
        .catch((_) => {
            console.warn('Unable to find existing data to append in %s', assetUrl)
            write(species, dir, fileName)
        })
}

async function write(data: any, dir: string, fileName: string) {
    fs.mkdirSync(dir, { recursive: true })
    const json = JSON.stringify(data, null, 2)
    fs.writeFileSync(`${dir}/${fileName}`, json, 'utf-8')
}

async function extractData(species: Species, pokemonAPIData: Pokemon | undefined, speciesAPIData: PokemonSpecies | undefined, locale: Map<Locale, Map<string, string>>) {
    const holder: stats.StatHolder = {
        name: extractName(species),
        nationalPokedexNumber: species.num,
        primaryType: species.types[0].toLocaleLowerCase(),
        secondaryType: species.types[1]?.toLocaleLowerCase(),
        abilities: extractAbility(species),
        baseStats: extractStatMap(species),
        catchRate: speciesAPIData?.capture_rate ?? 0,
        maleRatio: extractGenderRatio(species),
        baseExperienceYield: pokemonAPIData?.base_experience ?? 0,
        baseFriendship: speciesAPIData?.base_happiness ?? 0,
        evYield: {
            hp: pokemonAPIData?.stats.find(stat => stat.stat.name === 'hp')?.effort ?? 0,
            attack: pokemonAPIData?.stats.find(stat => stat.stat.name === 'attack')?.effort ?? 0,
            defence: pokemonAPIData?.stats.find(stat => stat.stat.name === 'defense')?.effort ?? 0,
            special_attack: pokemonAPIData?.stats.find(stat => stat.stat.name === 'special-attack')?.effort ?? 0,
            special_defence: pokemonAPIData?.stats.find(stat => stat.stat.name === 'special-defense')?.effort ?? 0,
            speed: pokemonAPIData?.stats.find(stat => stat.stat.name === 'speed')?.effort ?? 0
        },
        experienceGroup: speciesAPIData ? extractExperienceGroup(speciesAPIData) : 'slow',
        eggCycles: speciesAPIData?.hatch_counter ?? 0,
        eggGroups: species.eggGroups.map(eggGroup => eggGroup.toLocaleLowerCase().replace(/\s/g, '_').replace('-', '_')),
        moves: extractMoves(species),
        labels: extractLabels(species),
        aspects: extractAspects(species),
        height: species.heightm * 10,
        weight: species.weighthg,
        preEvolution: extractPreEvolution(species),
        evolutions: extractEvolutions(species),
        cannotDynamax: species.cannotDynamax
    }
    if (species.forme) {
        const data = formsHolder.get(toID(species.baseSpecies)) ?? []
        const form = holder as stats.Form
        form.nationalPokedexNumber = undefined
        form.battleOnly = species.battleOnly ? true : false
        data.push(form)
        formsHolder.set(toID(species.baseSpecies), data)
        console.info(`Finished ${species.baseSpecies} ${species.forme} form`)
    }
    else {
        const asSpecies: stats.Species = holder as stats.Species
        asSpecies.generation = species.gen
        speciesHolder.set(toID(species.baseSpecies), asSpecies)
        console.info(`Finished ${species.baseSpecies} species`)
    }
    if (speciesAPIData) {
        speciesAPIData.names.forEach(entry => appendLangEntry(entry.language.name, locale, `cobblemon.species.${toID(species.baseSpecies)}.name`, entry.name))
        speciesAPIData.flavor_text_entries.forEach(entry => appendLangEntry(entry.language.name, locale, `cobblemon.species.${toID(species.baseSpecies)}.desc`, entry.flavor_text.replace(/\n/g, ' ')))
    }
}

function toPokeApiID(text: string): string {
    return text.toLocaleLowerCase().replace(/[^a-z0-9 \s -]+/g, '').replace(/\s/g, '-')
}

function extractName(species: Species): string {
    return species.forme ? species.forme : species.name
}

function extractAbility(species: Species): stats.Ability[] {
    const data = [new stats.Ability(species.abilities[0], false)]
    if (species.abilities[1]) {
        data.push(new stats.Ability(species.abilities[1], false))
    }
    if (species.abilities.H) {
        data.push(new stats.Ability(species.abilities.H, true))
    }
    // We don't collect special abilities we want forms to use that format instead
    return data
}

function extractStatMap(species: Species): stats.StatMap {
    return {
        hp: species.baseStats['hp'],
        attack: species.baseStats['atk'],
        defence: species.baseStats['def'],
        special_attack: species.baseStats['spa'],
        special_defence: species.baseStats['spd'],
        speed: species.baseStats['spe']
    }
}

function extractGenderRatio(species: Species): number {
    if (species.gender === '') {
        return species.genderRatio.M
    }
    else if (species.gender === 'N') {
        return -1
    }
    else if (species.gender === 'M') {
        return 1
    }
    else {
        return 0
    }
}

function extractExperienceGroup(speciesAPIData: PokemonSpecies): stats.ExperienceGroup {
    switch (speciesAPIData.growth_rate.name) {
        case 'slow':
            return 'slow'
        case 'medium':
            return 'medium_fast'
        case 'fast':
            return 'fast'
        case 'medium-slow':
            return 'medium_slow'
        case 'slow-then-very-fast':
            return 'erratic'
        case 'fast-then-very-slow':
            return 'fluctuating'
        default:
            throw new Error(`Received invalid experience group named ${speciesAPIData.growth_rate.name}`)
    }
}

/**
 * This will map the learnset of a species to Cobblemon equivalents.
 * Things of note:
 * M move sources will always map to TMs, the concept of a technical disk will allow learning from any of the 3 known disks.
 * T, D, S and V all map to tutor, this is for the sake of simplicity and because we won't have an equivalent to a lot of forgotten concepts or Wi-Fi events.
 * E stays as an egg move.
 * R will be species depending, most of them are form change attacks but some are due to "glitches" such as gen 3 Ninjask x Shedinja move inheritance which will not be used.
 * We only use the most recent source for a specific move for example if tackle is learnt at level 5 in generation 1 but is instead an egg move in generation 2 we will use the generation 2 source.
 * This is done to have the most up to date representation of what a Pokémon would look like if they didn't lose moves across generations.
 * 
 * @param species The [Species] being queried.
 * @returns An array containing the learnset mapped to Cobblemon.
 */
function extractMoves(species: Species): stats.MoveSource[] | undefined {
    const cobblemonLearnset: stats.MoveSource[] = []
    const ignoredRestricted = ['shedinja']
    const showdownLearnset = Dex.species.getLearnset(species.id) || {}
    // Showdown doesn't keep track of LA moves however this is required for Stantler to evolve lol
    if (species.id === 'stantler') {
        cobblemonLearnset.push(new stats.LevelMoveSource('psyshieldbash', 21, 8))
    }
    for (const move in showdownLearnset) {
        const moveId = toID(move)
        for (const moveSource of showdownLearnset[moveId]) {
            const match = moveSource.match(/[0-9]+/g)
            if (match === null) {
                continue
            }
            const generation = +match[0]
            const currentSource = moveSource.replace(generation.toString(), '')
            if (currentSource.startsWith('M')) {
                cobblemonLearnset.push(new stats.TechnicalDiskMoveSource(moveId))
            }
            else if (currentSource.startsWith('L')) {
                const level = +currentSource.replace('L', '')
                // This is a move learnt on evolution
                if (level === 0) {
                    const propertyId = formatAsProperty(species).toJSON()
                    const holder = evolutionMovesHolder.get(propertyId) ?? []
                    holder.push(moveId)
                    evolutionMovesHolder.set(propertyId, holder)
                    continue
                }
                let inject = true;
                cobblemonLearnset.forEach(source => {
                    if (source.name === moveId && source instanceof stats.LevelMoveSource && source.generation <= generation) {
                        inject = false;
                        // We want the most recent version of level up to always overwrite previous.
                        source.generation = generation
                        if (source.level < level) {
                            source.level = level;
                        }
                    }
                })
                if (inject) {
                    cobblemonLearnset.push(new stats.LevelMoveSource(moveId, level, generation))
                }
            }
            else if (currentSource.startsWith('R')) {
                if (!ignoredRestricted.includes(species.baseSpecies)) {
                    cobblemonLearnset.push(new stats.FormChangeSource(moveId))
                }
            }
            else if (currentSource.startsWith('E')) {
                cobblemonLearnset.push(new stats.EggMoveSource(moveId))
            }
            else {
                cobblemonLearnset.push(new stats.TutorMoveSource(moveId))
            }
        }
    }
    const numericalEntries = cobblemonLearnset.filter(move => !isNaN(parseInt(move.toJSON())))
        .sort((a, b) => parseInt(a.toJSON()) - parseInt(b.toJSON()))
    const literalEntries = cobblemonLearnset.filter(move => !numericalEntries.includes(move))
        .sort((a, b) => a.toJSON().localeCompare(b.toJSON()))
    const merged = numericalEntries.concat(literalEntries)
    const returning = Array.from(new Map(merged.map(element => [element.toJSON(), element])).values())
    return returning.length ? returning : undefined
}

function extractLabels(species: Species): stats.Labels[] {
    const baseSpecies = Dex.species.get(species.baseSpecies)
    const labels = new Set<stats.Labels>([`gen${species.gen}`])
    if (species.isMega === true) {
        labels.add('mega')
    }
    if (species.isPrimal === true) {
        labels.add('primal')
    }
    if (species.tags.includes('Mythical')) {
        labels.add('mythical')
        labels.add('legendary')
    }
    // Miraidon and Koraidon aren't tagged on showdown despite being officially considered a Paradox too
    if (species.tags.includes('Paradox') || ['miraidon', 'koraidon'].includes(species.name)) {
        labels.add('paradox')
    }
    // "Cheat" for UB check
    if (species.abilities[0] === 'Beast Boost') {
        labels.add('ultra_beast')
    }
    if (species.tags.includes('Restricted Legendary') || (species.tags.includes('Sub-Legendary') && species.tags.length === 1 && !labels.has('paradox') && !labels.has('ultra_beast'))) {
        labels.add('legendary')
    }
    // From bulbapedia "fan term commonly used to refer to any Pokémon that has a three-stage evolution line, 1,250,000 experience at level 100, and a base stat total of exactly 600", we don't have access to the experience but the other 2 checks are enough
    if (baseSpecies.bst === 600 && baseSpecies.prevo && Dex.species.get(baseSpecies.prevo).prevo) {
        labels.add('pseudo_legendary')
    }// "Cheat" for UB check
    if (species.abilities[0] === 'Beast Boost') {
        labels.add('ultra_beast')
    }
    // Easier to just do this, too many things to check for
    if (BABY_POKEMON.includes(species.baseSpecies)) {
        labels.add('baby')
    }
    // Unnecessary extra checks but you never know, it might break one day
    if (species.forme.endsWith('Gmax') && species.changesFrom && Dex.species.get(species.changesFrom).canGigantamax) {
        labels.add('gmax')
    }
    // This may break one day but today is not that day
    if (species.forme.endsWith('Totem')) {
        labels.add('totem')
    }
    if (REGIONAL_FORMS.includes(species.forme)) {
        const label = regionalLabelFor(species)
        labels.add(label)
        // This is fine to do because showdown stores the base form before the regionals so it will always exist to label
        if (speciesHolder.get(toID(species.baseSpecies))) {
            const baseLabel = regionalLabelFor(baseSpecies)
            speciesHolder.get(toID(species.baseSpecies))?.labels.push(baseLabel)
        }
    }
    return Array.from(labels)
}

function extractAspects(species: Species): string[] {
    const aspects: string[] = []
    if (species.baseSpecies === 'Basculin') {
        switch (species.forme) {
            case 'Blue-Striped':
                aspects.push('bluestriped')
                break
            case 'White-Striped':
                aspects.push('whitestriped')
                break
            default:
                aspects.push('redstriped')
                break
        }
        return aspects
    }
    switch (species.forme) {
        case 'Mega':
            aspects.push('mega')
            break
        case 'Mega-X':
            aspects.push('mega-x')
            break
        case 'Mega-Y':
            aspects.push('mega-y')
            break
        case 'Alola':
            aspects.push('alolan')
            break
        case 'Galar':
            aspects.push('galarian')
            break
        case 'Hisui':
            aspects.push('hisuian')
            break
        case 'Paldea':
            aspects.push('paldean')
            break
        case 'Paldea-Aqua':
            aspects.push('paldean-breed-aqua')
            break
        case 'Paldea-Combat':
            aspects.push('paldean-breed-combat')
            break
        case 'Paldea-Blaze':
            aspects.push('paldean-breed-blaze')
            break
        case 'Paldea-Aqua':
            aspects.push('paldean-breed-aqua')
            break
        default:
            if (species.forme) {
                aspects.push(species.forme.toLocaleLowerCase())
            }
            break
    }
    return aspects
}

function extractPreEvolution(species: Species): stats.PokemonProperties | undefined {
    if (species.prevo === '') {
        return undefined
    }
    const preEvoSpecies = Dex.species.get(species.prevo)
    if (!preEvoSpecies.exists) {
        return undefined
    }
    return preEvoSpecies.forme === '' ? new stats.PokemonProperties(toID(preEvoSpecies.baseSpecies)) : new stats.PokemonProperties(toID(preEvoSpecies.baseSpecies), `form=${toID(preEvoSpecies.forme)}`)
}

function extractEvolutions(species: Species): stats.Evolution[] {
    const evolutions: stats.Evolution[] = []
    for (const evolutionSpeciesName of species.evos) {
        const evolutionSpecies = Dex.species.get(evolutionSpeciesName)
        if (!evolutionSpecies.exists) {
            continue
        }
        const source = toID(species.baseSpecies)
        const result = toID(evolutionSpecies.baseSpecies)
        const resultProperty = formatAsProperty(evolutionSpecies)
        if (evolutionSpecies.evoType === 'trade') {
            const tradeEvolution = new stats.TradeEvolution(source, result, resultProperty, [])
            if (evolutionSpecies.evoItem) {
                tradeEvolution.requirements.push(new stats.HeldItemRequirement(createCobbledItem(evolutionSpecies.evoItem)))
                tradeEvolution.consumeHeldItem = true
            }
            // This is only for karrablast/shelmet so it's safe to just do a lazy replace
            if (evolutionSpecies.evoCondition) {
                tradeEvolution.tradePartner = toID(evolutionSpecies.evoCondition.replace('with a ', ''))
            }
            appendRequirementsForCondition(tradeEvolution, evolutionSpecies)
            evolutions.push(tradeEvolution)
        }
        else if (evolutionSpecies.evoType === 'useItem') {
            if (!evolutionSpecies.evoItem) {
                throw new Error('useItem without an evoItem (?)')
            }
            const evolution = new stats.IdentifierInteractionEvolution(source, result, resultProperty, createCobbledItem(evolutionSpecies.evoItem), [], false, true)
            appendRequirementsForCondition(evolution, evolutionSpecies)
            evolutions.push(evolution)
        }
        else if (evolutionSpecies.evoType === 'levelMove') {
            if (!evolutionSpecies.evoMove) {
                throw new Error('levelMove without an evoMove (?)')
            }
            const evolution = new stats.PassiveEvolution(source, result, resultProperty, [new stats.MoveRequirement(evolutionSpecies.evoMove)], false, true)
            appendRequirementsForCondition(evolution, evolutionSpecies)
            evolutions.push(evolution)
        }
        else if (evolutionSpecies.evoType === 'levelExtra') {
            const levelEvolution = new stats.PassiveEvolution(source, result, resultProperty, [], false, true)
            // Probopass: we translate to modern evolution Magnezone who shared this method uses
            if (evolutionSpecies.evoCondition === 'near a special magnetic field') {
                evolutions.push(new stats.IdentifierInteractionEvolution(source, result, resultProperty, new stats.Identifier('cobblemon', 'thunder_stone'), [], false, true))
                continue
            }
            appendRequirementsForCondition(levelEvolution, evolutionSpecies)
            evolutions.push(levelEvolution)
        }
        else if (evolutionSpecies.evoType === 'levelFriendship') {
            const evolution = new stats.PassiveEvolution(source, result, resultProperty, [new stats.IntRequirement('friendship', 160)], false, true)
            appendRequirementsForCondition(evolution, evolutionSpecies)
            evolutions.push(evolution)
        }
        else if (evolutionSpecies.evoType === 'levelHold') {
            if (!evolutionSpecies.evoItem) {
                throw new Error('levelHold without an evoItem (?)')
            }
            const evolution = new stats.PassiveEvolution(source, result, resultProperty, [new stats.HeldItemRequirement(createCobbledItem(evolutionSpecies.evoItem))], true, true)
            appendRequirementsForCondition(evolution, evolutionSpecies)
            evolutions.push(evolution)
        }
        else if (evolutionSpecies.evoType === 'other') {
            if (evolutionSpecies.evoCondition === 'Defeat the Single Strike Tower') {
                evolutions.push(new stats.IdentifierInteractionEvolution(source, result, resultProperty, createCobbledItem('Scroll of Darkness'), [], false, false))
                continue
            }
            else if (evolutionSpecies.evoCondition === 'Defeat the Rapid Strike Tower') {
                evolutions.push(new stats.IdentifierInteractionEvolution(source, result, resultProperty, createCobbledItem('Scroll of Waters'), [], false, false))
                continue
            }
            else if (evolutionSpecies.evoCondition === 'Black Augurite') {
                evolutions.push(new stats.IdentifierInteractionEvolution(source, result, resultProperty, createCobbledItem('Black Augurite'), [], false, true))
                continue
            }
            else if (evolutionSpecies.evoCondition === 'Peat Block when there\'s a full moon') {
                evolutions.push(new stats.IdentifierInteractionEvolution(source, result, resultProperty, createCobbledItem('Peat Block'), [new stats.MoonPhaseRequirement('FULL_MOON'), new stats.TimeRangeRequirement('night')], false, true))
                continue
            }
            const evolution = new stats.PassiveEvolution(source, result, resultProperty, [], false, true)
            appendRequirementsForCondition(evolution, evolutionSpecies)
            evolutions.push(evolution)
        }
        else {
            // If we ever need maxLevel too this needs to have a dedicated level requirement implementation
            const evolution = new stats.PassiveEvolution(source, result, resultProperty, [new stats.IntRequirement('level', evolutionSpecies.evoLevel!!, 'minLevel')], false, true)
            if (evolutionSpecies.evoItem) {
                console.warn(`Unexpected ${evolutionSpecies.evoItem} for ${evolutionSpecies.name}`)
            }
            if (evolutionSpecies.evoMove) {
                console.warn(`Unexpected ${evolutionSpecies.evoMove} for ${evolutionSpecies.name}`)
            }
            if (evolutionSpecies.evoRegion) {
                // TODO This will be a decision down the line if we follow up with "genetic" traits determining some things related to what the games assign to regions
            }
            appendRequirementsForCondition(evolution, evolutionSpecies)
            evolutions.push(evolution)
        }
    }
    return evolutions
}

function formatAsProperty(species: Species): stats.PokemonProperties {
    return species.forme ? new stats.PokemonProperties(toID(species.baseSpecies), `form=${toID(species.forme)}`) : new stats.PokemonProperties(toID(species.baseSpecies))
}

function createCobbledItem(path: string): stats.Identifier {
    if (path === 'Up-Grade') {
        return new stats.Identifier('cobblemon', 'upgrade')
    }
    return new stats.Identifier('cobblemon', path.toLocaleLowerCase().replace('-', '_').replace('\'', '').replace(/\s/g, '_'))
}

function appendRequirementsForCondition(evolution: stats.Evolution, evolutionSpecies: Species) {
    if (!evolutionSpecies.evoCondition) {
        return
    }
    const requirements = CONDITION_TO_REQUIREMENT.get(evolutionSpecies.evoCondition.toLocaleLowerCase())
    if (requirements === undefined) {
        throw new Error(`Cannot find requirements for condition ${evolutionSpecies.evoCondition}`)
    }
    evolution.requirements = evolution.requirements.concat(requirements)
}

function regionalLabelFor(species: Species): stats.Labels {
    if (species.forme === 'Hisui') {
        return 'hisui_regional'
    }
    switch (species.gen) {
        case 1:
            return 'kanto_regional'
        case 2:
            return 'johto_regional'
        case 3:
            return 'hoenn_regional'
        case 4:
            return 'sinnoh_regional'
        case 5:
            return 'unova_regional'
        case 6:
            return 'kalos_regional'
        case 7:
            return 'alola_regional'
        case 8:
            return 'galar_regional'
        case 9:
            return 'paldea_regional'
        default:
            throw new Error(`Cannot resolve regional label for ${species.name}`)
    }
}

function appendEvolutionMoves(statHolder: Partial<stats.StatHolder>) {
    statHolder.evolutions?.forEach(evolution => {
        const moves = evolutionMovesHolder.get(evolution.resultingProperty.toJSON())
        if (moves !== undefined) {
            evolution.learnableMoves = Array.prototype.concat(evolution.learnableMoves, moves)
        }
    })
}

function resolveAshGreninja(greninja: stats.Species) {
    greninja.forms?.push({
        name: 'Base Ash',
        labels: greninja.labels,
        abilities: [new stats.Ability('Battle Bond', false)],
        battleOnly: false
    })
}

function resolveZygardePowerConstruct(zygarde: stats.Species) {
    const completeFifty: stats.Form = {
        name: 'Complete 50%',
        abilities: [new stats.Ability('Power Construct', false)],
        battleOnly: false
    }
    console.log(zygarde.forms)
    // We want a deep copy since we don't want to inherit anything from base (50%)
    const json = JSON.stringify(zygarde.forms?.find(form => form.name === '10')!!)
    console.log(json)
    const completeTen: stats.Form = JSON.parse(json)
    completeTen.name = 'Complete 10%'
    completeTen.abilities = [new stats.Ability('Power Construct', false)]
    zygarde.forms?.push(completeFifty)
    zygarde.forms?.push(completeTen)
}

function resolveOwnTempoRockruff(rockruff: stats.Species) {
    const clone = Object.create(rockruff) as stats.Form
    clone.name = 'Own Tempo'
    clone.abilities = [new stats.Ability('Own Tempo', false)]
    clone.evolutions = clone.evolutions?.filter(evolution => evolution.resultingProperty.toJSON() === 'lycanroc form=dusk')
    rockruff.evolutions = rockruff.evolutions?.filter(evolution => evolution.resultingProperty.toJSON() !== 'lycanroc form=dusk')
    const existing = rockruff.forms ?? []
    existing.push(clone)
    rockruff.forms = existing
}

function resolveDuskLycanroc(lycanroc: stats.Species) {
    lycanroc.forms?.find(form => form.name === 'Dusk')?.preEvolution?.args?.push('form=own_tempo')
}