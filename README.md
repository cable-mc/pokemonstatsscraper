# PokemonStatsScraper

Scrapes Pokémon data from [Pokémon Showdown](https://github.com/smogon/pokemon-showdown) and [PokéAPI](https://pokeapi.co/) in order to create the data representation of Pokémon species data for the [Cobblemon](https://cobblemon.com/) mod.
This scraper will automatically append data to our existing assets allowing you to only generate relevant species data without losing our hand crafted drops, hitbox sizes, species features and many other Cobblemon only attributes.

## Output

The generated data can be found under the `data` folder.
