import { toID } from '@pkmn/sim';

export interface StatHolder {
    name: string
    nationalPokedexNumber: number
    primaryType: string
    secondaryType?: string
    abilities: Ability[]
    baseStats: StatMap
    catchRate: number
    maleRatio: number
    baseExperienceYield: number
    baseFriendship: number
    evYield: StatMap
    experienceGroup: ExperienceGroup
    eggCycles: number
    eggGroups: string[]
    moves?: MoveSource[]
    labels: Labels[]
    aspects: string[]
    height: number
    weight: number
    preEvolution?: PokemonProperties
    evolutions: Evolution[]
    cannotDynamax?: boolean
}

export interface Species extends StatHolder {
    
    // always undefined before writting to disk
    generation?: number
    forms?: Form[]

}

export interface Form extends Partial<StatHolder> {

    battleOnly: boolean

}

export type Labels = `gen${number}` | 'pseudo_legendary' | 'legendary' | 'baby' | 'mythical' | 'mega' | 'primal' | 'ultra_beast' | 'totem' | 'gmax' | 'paradox'
    | 'kanto_regional' | 'johto_regional' | 'hoenn_regional' | 'sinnoh_regional' | 'unova_regional' | 'kalos_regional' | 'alola_regional' | 'galar_regional' | 'hisui_regional' | 'paldea_regional'

export type Stat = 'hp' | 'attack' | 'defence' | 'special_attack' | 'special_defence' | 'speed'

export type ExperienceGroup = 'slow' | 'medium_fast' | 'fast' | 'medium_slow' | 'erratic' | 'fluctuating'

export type StatMap = {[stat in Stat]: number}

export class Ability {
    name: string
    hidden: boolean

    constructor(name: string, hidden: boolean) {
        this.name = toID(name)
        this.hidden = hidden
    }
    
    toJSON(): string {
        return this.hidden ? `h:${this.name}` : this.name
    }
}

export interface MoveSource {
    name: string

    toJSON(): string
}

export class LevelMoveSource implements MoveSource {
    name: string
    level: number
    /**
     * Not used in Cobblemon itself just a way for us to ensure it's not only the highest but also the most recent
     */
    generation: number

    constructor(name: string, level: number, generation: number) {
        this.name = toID(name)
        this.level = level
        this.generation = generation
    }

    toJSON(): string {
        return `${this.level}:${this.name}`
    }
}

export class TutorMoveSource implements MoveSource {
    name: string

    constructor(name: string) {
        this.name = toID(name)
    }

    toJSON(): string {
        return `tutor:${this.name}`
    }
}

export class TechnicalDiskMoveSource implements MoveSource {
    name: string

    constructor(name: string) {
        this.name = toID(name)
    }

    toJSON(): string {
        return `tm:${this.name}`
    }
}

export class EggMoveSource implements MoveSource {
    name: string

    constructor(name: string) {
        this.name = toID(name)
    }

    toJSON(): string {
        return `egg:${this.name}`
    }
}

export class FormChangeSource implements MoveSource {
    name: string

    constructor(name: string) {
        this.name = toID(name)
    }

    toJSON(): string {
        return `form_change:${this.name}`
    }
}

export class PokemonProperties {
    species: string
    args: PokemonPropertiesArgument[]
    constructor(species: string, ...args: PokemonPropertiesArgument[]) {
        this.species = species
        this.args = args
    }

    toJSON(): string {
        return this.args.length > 0 ? `${this.species} ${this.args.join(' ')}` : this.species
    }
}

export type PokemonPropertiesArgument = `${string}=${string}`

export interface EvolutionLike {

    variant: string

}

export type EvolutionRequirement = IntRequirement | BiomeRequirement | TimeRangeRequirement | HeldItemRequirement | StatRatioRequirement | PartyMemberRequirement
    | PropertiesRequirement | MoveRequirement | MoveTypeRequirement | MoonPhaseRequirement | UseMoveRequirement | DefeatRequirement

export class IntRequirement implements EvolutionLike {

    variant: string
    amount: number
    amountPropertyName: string

    constructor(variant: string, amount: number, amountPropertyName: string = 'amount') {
        this.variant = variant
        this.amount = amount
        this.amountPropertyName = amountPropertyName
    }

    toJSON() {
        return Object.fromEntries(new Map<string, any>([
            ['variant', this.variant],
            [this.amountPropertyName, this.amount]
        ]))
    }

}

export class BiomeRequirement implements EvolutionLike {

    variant: string = 'biome'
    biomeCondition: string

    constructor(biome: string) {
        this.biomeCondition = biome
    }

}

export type TimeRange = 'any' | 'day' | 'night' | 'noon' | 'midnight' | 'dawn' | 'dusk' | 'twilight' | 'morning' | 'afternoon'

export class TimeRangeRequirement implements EvolutionLike {

    variant: string = 'time_range'
    range: TimeRange

    constructor(range: TimeRange) {
        this.range = range
    }

}

export class HeldItemRequirement implements EvolutionLike {

    variant: string = 'held_item'
    itemCondition: Identifier

    constructor(item: Identifier) {
        this.itemCondition = item
    }

}

export type StatRatio = 'ATTACK_HIGHER' | 'EQUAL' | 'DEFENCE_HIGHER'

export class StatRatioRequirement implements EvolutionLike {

    variant: string = 'attack_defence_ratio'
    ratio: StatRatio

    constructor(ratio: StatRatio) {
        this.ratio = ratio
    }

}

export class PartyMemberRequirement implements EvolutionLike {

    variant: string = 'party_member'
    target: string
    contains: boolean

    constructor(property: string, contains: boolean) {
        this.target = property
        this.contains = contains
    }

}

export class PropertiesRequirement implements EvolutionLike {

    variant: string = 'properties'
    target: string

    constructor(property: string) {
        this.target = property
    }

}

export class MoveRequirement implements EvolutionLike {

    variant: string = 'has_move'
    move: string

    constructor(move: string) {
        this.move = toID(move)
    }

}

export class UseMoveRequirement implements EvolutionLike {

    variant: string = 'use_move'
    move: string
    amount: number

    constructor(move: string, amount: number) {
        this.move = toID(move)
        this.amount = amount
    }

    toJson() {
        return {
            move: this.move,
            amount: this.amount
        }
    }

}

export class MoveTypeRequirement implements EvolutionLike {

    variant: string = 'has_move_type'
    type: string

    constructor(type: string) {
        this.type = toID(type)
    }

}
 
export type MoonPhase = 'FULL_MOON' | 'WANING_GIBBOUS' | 'THIRD_QUARTER' | 'WANING_CRESCENT' | 'NEW_MOON' | 'WAXING_CRESCENT' | 'FIRST_QUARTER' | 'WAXING_GIBBOUS'

export class MoonPhaseRequirement implements EvolutionLike {

    variant: string = 'moon_phase'
    moonPhase: MoonPhase

    constructor(moonPhase: MoonPhase) {
        this.moonPhase = moonPhase
    }

}

export class DefeatRequirement implements EvolutionLike {

    variant: string = 'defeat'
    target: PokemonProperties
    amount: number

    constructor(target: PokemonProperties, amount: number) {
        this.target = target
        this.amount = amount
    }

}

export interface Evolution extends EvolutionLike {

    variant: string
    sourceSpecies: string
    resultingSpecies: string
    resultingProperty: PokemonProperties
    requirements: EvolutionRequirement[]
    consumeHeldItem: boolean
    learnableMoves: string[]

    toJSON(): { [k: string]: any }

}

export class PassiveEvolution implements Evolution {

    variant: string
    sourceSpecies: string
    resultingSpecies: string
    resultingProperty: PokemonProperties
    requirements: EvolutionRequirement[]
    consumeHeldItem: boolean
    learnableMoves: string[] = []

    constructor(sourceSpecies: string, resultingSpecies: string, resultingProperty: PokemonProperties, requirements: EvolutionRequirement[], consumeHeldItem: boolean, levelUp: boolean) {
        this.variant = levelUp ? 'level_up' : 'passive'
        this.sourceSpecies = sourceSpecies
        this.resultingSpecies = resultingSpecies
        this.resultingProperty = resultingProperty
        this.requirements = requirements
        this.consumeHeldItem = consumeHeldItem
    }

    toJSON(): { [k: string]: any } {
        return Object.fromEntries(new Map<string, any>([
            ['id', `${this.sourceSpecies}_${this.resultingSpecies}`],
            ['variant', this.variant],
            ['result', this.resultingProperty],
            ['consumeHeldItem', this.consumeHeldItem],
            ['learnableMoves', this.learnableMoves],
            ['requirements', this.requirements]
        ]))
    }
    
}

export class IdentifierInteractionEvolution implements Evolution {

    variant: string
    sourceSpecies: string
    resultingSpecies: string
    resultingProperty: PokemonProperties
    requirements: EvolutionRequirement[]
    consumeHeldItem: boolean
    target: Identifier
    learnableMoves: string[] = []

    constructor(sourceSpecies: string, resultingSpecies: string, resultingProperty: PokemonProperties, target: Identifier, requirements: EvolutionRequirement[], consumeHeldItem: boolean, itemInteraction: boolean) {
        this.sourceSpecies = sourceSpecies
        this.resultingSpecies = resultingSpecies
        this.resultingProperty = resultingProperty
        this.target = target
        this.requirements = requirements
        this.consumeHeldItem = consumeHeldItem
        this.variant = itemInteraction ? 'item_interact' : 'block_click'
    }

    toJSON(): { [k: string]: any } {
        return Object.fromEntries(new Map<string, any>([
            ['id', `${this.sourceSpecies}_${this.resultingSpecies}`],
            ['variant', this.variant],
            ['result', this.resultingProperty],
            ['consumeHeldItem', this.consumeHeldItem],
            ['learnableMoves', this.learnableMoves],
            ['requirements', this.requirements],
            [this.variant === 'block_click' ? 'block' : 'requiredContext', this.target]
        ]))
    }
    
}

export class TradeEvolution implements Evolution {

    variant: string = 'trade'
    sourceSpecies: string
    resultingSpecies: string
    resultingProperty: PokemonProperties
    requirements: EvolutionRequirement[]
    consumeHeldItem: boolean
    tradePartner?: string
    learnableMoves: string[] = []

    constructor(sourceSpecies: string, resultingSpecies: string, resultingProperty: PokemonProperties, requirements: EvolutionRequirement[], consumeHeldItem: boolean = false, tradePartner?: string) {
        this.sourceSpecies = sourceSpecies
        this.resultingSpecies = resultingSpecies
        this.resultingProperty = resultingProperty
        this.tradePartner = tradePartner
        this.requirements = requirements
        this.consumeHeldItem = consumeHeldItem
    }

    toJSON(): { [k: string]: any } {
        const map = new Map<string, any>([
            ['id', `${this.sourceSpecies}_${this.resultingSpecies}`],
            ['variant', this.variant],
            ['result', this.resultingProperty],
            ['consumeHeldItem', this.consumeHeldItem],
            ['learnableMoves', this.learnableMoves],
            ['requirements', this.requirements]
        ])
        if (this.tradePartner) {
            map.set('requiredContext', this.tradePartner)
        }
        return Object.fromEntries(map)
    }
    
}

export class Identifier {

    namespace: string
    path: string

    constructor(namespace: string, path: string) {
        if (!/[a-z0-9_.-]/.test(namespace)) {
            throw new Error('Non [a-z0-9_.-] character in namespace of identifier')
        }
        if (!/[a-z0-9\/._-]/.test(path)) {
            throw new Error('Non [a-z0-9/._-] character in path of identifier')
        }
        this.namespace = namespace.toLocaleLowerCase()
        this.path = path.toLocaleLowerCase()
    }

    toJSON(): string {
        return `${this.namespace}:${this.path}`
    }

}